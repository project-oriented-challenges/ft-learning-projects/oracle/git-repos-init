#!/usr/bin/env python

import csv

# The script expects the csv file that keeps informaton about repos in form of
# <group-of-participants>

META_GROUP="onti-fintech/onti-2021-fintech/"

exec_file = "#!/bin/bash\n\n"

with open('repos.csv') as csvfile:
    d = csv.reader(csvfile, delimiter=";")
    for row in d:
        print(row)
        gr   = row[0]
        exec_file+=f'echo "generate repo for {gr}"\n'
        exec_file+="cp -a template repo\n"
        exec_file+="cd repo\n"
        exec_file+="git init\n"
        exec_file+=f'git remote add origin git@gitlab.com:{META_GROUP}{gr}/oracle.git\n'
        exec_file+="git add .\n"
        exec_file+="git commit -m 'Initial commit from template [skip ci]'\n"
        exec_file+="git push -u origin master\n"
        exec_file+="cd ..\n"
        exec_file+="rm -rf repo\n\n"
    
with open('init-repos.sh', 'w') as script_file:
    script_file.write(exec_file)
