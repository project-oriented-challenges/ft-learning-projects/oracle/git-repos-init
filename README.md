Repos generation
====

1. Prepare the csv file `repos.csv` that keeps informaton about repos of participants. Every line is the team id. For example,
    ```
    fintech2021001
    fintech2021002
    fintech2021003
    ```

2. Run `python 01-generate-init-script.py`

3. Run generated init script: `bash ./init-repos.sh`